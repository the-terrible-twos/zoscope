## Week 13

+ # Jan 9 Tue
- Project selection was made within the team.
- Created a new group on the GitLab, forked the repo under our group.
- Cloned the repo to work locally.
- Started working for Wireframes within the group.

+ # Jan 10 Wed
- Contiuned working with Wireframes within the team.
- Had existing docker containers up and running.

+ # Jan 11 Thur
- Contiuned working with Wireframes within the team for last touch.
- Which endpoints we needed were determined and distributed to each person.
- I started working with IP endpoints of GET-About Us and GET-My Pets using by FASTAPI.

+ # Jan 12 Fri
- Completed wireframe within the team.
- I contiuned working with IP endpoints of GET-About Us, GET-My Pets, POST-Create a new pet that belong a user, GET-Horoscope details of a pet using FASTAPI.
- Completed working with IPA Endpoints design and attached in team google doc.
- Pushed the current journal file to my own branch.

## Week 14

+ # Jan 16 Tue
- The first week presentation was made and feedback was received. Accordingly, endpoints were repaired.
- Preliminary work was done to prepare the table, user authentication, and setup database system.

+ # Jan 17 Wed
- We decided which database system we would use for the project within the team.
- After this PostgreSQL database system created and integrated into Docker.
- Downloaded and set up Database Managing Application Beekeeper

+ # Jan 18 Thur
- Started working with Backend user authentication to give access LogIn, LogOut, and SignUp within the group
- We had some errors on back end authentication, worked on it to run as expected.
- Created and migrated tables that names Users, and Pets
- We became ready to work on beck end developments by creating and migrating table
- Today we generally worked with common screen sharing.

+ # Jan 19 Fri
- We continue to work on our blocker that was back end authentication within the group. We want to send user's birthdays into the data. Generally we had error to troubleshoot was a Type error "Object of type date is not JSON serializable".
- To solve our issue, we received advice that having two different user tables to create a user registration. One of the tables will store user general information,
  other one will store birthday informations of users. The two tables will be linked together with foreign key.
- We will decide to create two separete tables to keep informations of users within the team.
- I examined different resources to solve the problem.

+ # Jan 20 Sat
- Worked and solved the blocker from yesterday. All functions are working for back end authentication such as SignUp, Login, Logout, Protected Account, and
  Get Token.
- There is no need to create two tables for the user - Code pushed to my branch and merged to main.

## Week 15

+ # Jan 22 Mon
- Started working with fastAPI endpoint to create a new pet that belongs to the user who is a member.
- Succesfully created IPA endpoint to create a pet
- Shared within the group by merging in main on GitLab
- Started working with another fastAPI endpoint to list pets that belong to the users.

+ # Jan 23 Tue
- Continued and completed working with FastAPI endpoint to list pets that belongs to the user who is member.
- Created new issues which are Create A New Pet-FastAPI End Point(#1) and  List my Pets-FastAPI End Point(#2) on the Gitlab
- Edited my last merge request that is Create pet branch with MR requirements checklist. Added Dev Summary and Test Plan sections. Linked Issue link #1 to request merge Create pet branch.


+ # Jan 24 Wed
- This morning we merged and pull all our codes into our local machines. Linked Issue link #2 to request merge List pets backend branch.
- Started working with Front End React to view of pets that belong users.
- Completed Front End React page of list my pets. It will entegreted to my profile page.
- Created new issue that List My Pets React(#7) on the GitLab

+ # Jan 25 Thur
- This afternoon I merged to main Front End React-List my pets that I completed yesterday.
- Then I worked on the front end React form to create a new pet.
- This form will allow users to add new pets into their profile.

+ # Jan 26 Fri
- Today I continued working on the React Form to create new pet that users may want to add their profile.
- A function that assigns a birth date has been integrated for users who do not know their pet's birthday.
- Completed React Form to create a nep pet. Tested by listing the created pet in the user interface manually.


## Week 15

+ # Jan 29 Mon
- Today merge requests completed within the group for last updates.
- I created About Us page on React Front End. The page structure has been created, the about us section will be written later.
- Started working with Unit Test for Back end unit tests for create a new pet and Get a Pet functions.
- Started working on Front End of Horoscope Details of pets that navigated from list pets of users. Functionality working with pet_id and current date.


+ # Jan 30 Tue
- Continued working on React Front End for Horoscope Details of pets that navigated from list pets of users.
- Completed fortune results page and merged to main on GitLab. Linked issue #22 for merged repo.
- Working on the fortune result page to shown up pet's name, starsign and image on the page.
- Contiuned and watch recording videos to create Unit Test for back end functionality.


+ # Jan 31 Wed
- This morning merged to main updates from my side such as React Front End pages and journal.
- Continued working on the Unit Test for Creat Pet and Get A Pet fastAPI endpoints.
- Towards the end of the day, the unit tests are completed and passed.


+ # Feb 1 Thur
- This afternoon I merged the unit tests of create a new pet and select a pet FastAPI endpoints to main on GitLab.
- Worked on the stretch goal for upload images to the cloud system.


+ # Feb 5 Mon
- My section of the Read Me file has been completed for ghi and api
- I was tried to help with the front end page of one of my partners.

+ # Feb 6 Tue
- Today I worked on the styling create pet form, pet lists views of user's profile.
- The final touches were made for development.
- All functions and views have been made ready for development.

+ # Feb 7 Wed
- The final touches were made for development.
- Today we worked on the development database and back end within the group shared screen.

+ # Feb 8 Thur
- Today worked on deployment backend and frontend, solved pipiline issues
- End of the day we were ready for gradig presentation and submit the project.

+ # Feb 9 Fri
- Today we presented the tasks we worked on individually.
- I updated my journal for last version before submit.
