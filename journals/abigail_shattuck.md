## Week 13

- Jan 9 Tue - Brainstormed project ideas, came up with several potential ones, will vote tommorrow
- Jan 10 Wed - Decided on Zoscope: horoscopes for pets, began wireframing
- Jan 11 Thur - Randomly split up API endpoints between the group, I ended up with signup, completed API endpoint
- Jan 12 Fri - Brainstormed additional features, ran into a few complications with instagram sharing, moving to stretch goal for now

## Week 14

- Jan 16 Tue - Cleaned up wireframe for presentation, added API endpoints
- Jan 17 Wed - Reallocated API endpoints and assigned jobs off of presentation critiques, started authentication
- Jan 18 Thur - Authentication group work, running into input/output errors
- Jan 19 Fri - Nav.js complete, authentication still causing issues, continuing to work together to fix bugs

## Week 15

- Jan 22 Mon - Authentication complete and merged to main, working signup front and back end, having quite a few issues with my docker containers after merging authentication branch, ended up having to downgrade docker version and rebuild containers
- Jan 23 Tue - working signup backend and front end, completed rough SPA's signup form front end, nav bar, home page, my profile. Ran into some issues with react-router-dom had to reinstall a newer version in ghi directory. Signup with auth front end and backend complete: tested with front end form and fastapi/beekeeper.
- Jan 17 Wed - Merged all changes for create/edit pet and login, started working lunar data back end api, created lunar calendar table
- Jan 18 Thur - Changed open API to rapidAPI moon phases, decided to do it all on the front end instead of storing data in database, created endpoint and successfully pulled lunar calendar and rendered it on page. Will need to work with the format once we switch to frontend work.
- Jan 19 Fri - ////////

## Week 16

- Jan 29 Mon - Finished lunar calendar and merged with main, pulled all teams changes, started on pet/human starsign , working on building table with compatibility percentages and a relevant message, once draft is completed I'll insert the data in the database, and start writing the backend form for H/P compatibility
- Jan 30 Tue - Finished backend for human-pet compatibility, successfully created and stored compatibility message with user_id and pet_id in compatibility table/db
- Jan 31 Wed - Worked on frontend for human pet compatibility, created form that shows Hello user_firstname message and drop down to select pet, but running into issues fetching user data, need to work out how to automatically show user data from logged in user
- Feb 01 Thur - Working on signup error handling and popups, still working front end for human pet compatibility, running into some blockers but making progress.
- Feb 02 Fri - ////////

## Week 17

- Feb 05 Mon - Successfully connected front and backend for human pet compatibility, utilizing the users logged in data and created pets to generate a compatibility message
- Feb 06 Tue - Merged all changes from main, added compatibility to nav bar for logged in users, started styling to ensure all formatting is uniform across the application, adding notes to README, and finished unit test. Unit test passed pytest.
- Feb 07 Wed - Began Backend development, unit tests passed, working through lint issues, finished back end deployment
- Feb 08 Thur -Began frontend deployment, worked through a lot of issues but successfully deployed application
- Feb 09 Fri - Presented final product to Paul, working on completing deployed starsign database, ensure styling is cohesive for all features. Turn in at EOD!!
