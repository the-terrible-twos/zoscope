## Week 17
+ Feb 5 - Feb 9
This week we spent a lot of time mob programming for deployment, as well as tying up some loose ends as well as styling and polishing to get the website uniform. Deployment was a huge challenge for us, but after two days straight of mob programming and a lot of help from SEIRS, we were finally able to get both our front and back ends deployed and live. We then had numerous bug fixes that we had to address once we had our site deployed. These bugs ranged from images not displaying properly, to full functionality of certain features not working properly. After lots of perseverance, our site is fully deployed and ready for submission. I cant be prouder of this group and the project we have built. Every time I look at it, it blows my mind more and more that we built this.

## Week 16
+ Jan 29 - Feb 1
This week I was able to get SendGrid set up as well as a redirect feature on our login page. With SendGrid, that was the first time ive used a 3rd party API into a fast API project. It was very challenging, however it was very cool and informative to get that process to work as intended. The other big win for me was setting up a redirect feature for our log in form. This shouldn't have been as difficult as it was, but with how the log in function with in jwt-down is written, it made it very difficult to find something to listen for to confirm log in status. After a little over a day of trial and error, I learned about the useRef hook, which can store a value value that doesn't make the page re-render.

## Week 15
+ Jan 22 - Jan 26
This week I was able to get my backend delete pet endpoint created. Also, I was able to build out a login front end and get it hooked up to our backend login endpoint. Lastly, I built out and styled a collapsible sidebar that will be used as the primary navigation of the page. Building out the sidebar was very fun, as it was the first time I got to use a react component, and see how its used and implemented across all pages.

## Week 14
+ Jan 16 - Jan 19
The main thing we got done this week was our authentication done. We were also able to get our DB set up with all the proper tables we will need for our project. Lastly, we started building out some of our front end pieces like the nav bar. We were able to complete all of the checklist points for the week. Next week we are planning to have all of our end points done, as well as building out more of the front end of the application.

## Week 13
+ Jan 8 - Jan 12
This week has mostly been set up and planning for the project. As a group, we were able to decide on a project idea,
complete our wireframe diagram, complete our MVP form, and finish our api endpoint planning. I also have my docker containers
set up and ready for when we start building out the application. So far I Feel like we are in a very good spot and am excited
to start building out the project next week.
