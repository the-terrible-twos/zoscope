## Week 17
+ Feb 9 Fri - Spent decent amount of time testing the backend and frontend functionality post-deployment (yesterday) and presentation (today)
            - Prepared presentation
            - Discussed with team how we are going to handle the starsign data table issue (either going to use migrations or pg admin)
            - Presented our project as a group to instructor
            - Code cleanup
            - README.md and docs folder final touches and cleanup

+ Feb 8 Thu - Worked on finishing backend deployment with team; started and finished frontend deployment with team
            - Spent all day with team troubleshooting issues related to frontend deployment, particularly CORS errors. Also had some errors with third-party API keys that we needed to solve together as a team. One of them involved putting the API key as a variable within our GitLab project and then unchecking the Protected option (this was for our API key to the lunar calendar).
            - Updated local branch with tweaks to the README.md, pulled latest changes from main (after deployment complete), merged into local branch

+ Feb 7 Wed - Worked on backend deployment all day with team; involved troubleshooting lots of errors given by flake8 and eslint, build-api-image job and api-unit-test jobs. Many failed pipelines that we were able to then fix through working through each of the errors one by one as a team. Code cleanup.

+ Feb 6 Tues - Finished unit testing, worked on front end styling
             - Started backend deployment with team
             - Installed and ran flake8; modified "pets" files (3) to respond to flake8 suggestions and completed merge request to main of those files during team
                screenshare
            - Spent chunk of time troubleshooting an array callback error from eslint; resulted in a solution where we had eslint intentionally disable then re-enable on specific lines of code (because we determined with a SEIR's help that the map function was working correctly and returning the values it needed to, eslint was just still hung up on the error)

+ Feb 5 Mon - Discussed updates with team and our plan for the week at standup
            - Reviewed unit testing and retested to verify all unit tests still passing with new pulls from main made
            - Screenshared merge request with teammate to review and approve their frontend and styling changes
	        - Pulled latest changes from main to further test frontend
            - Reran backend tests on fastAPI endpoints
            - Discussed deployment plans for tomorrow with teammates; reviewed list of steps we need to take to start backend deployment tomorrow
            - Ran through steps given in Learn to download the software necessary to start deployment tomorrow; troubleshooted security/ privacy issue with the download to finally get it to work

## Week 16
+ Feb 1 Thu     - In the course of working on the README datamodel.md, found some behavior in the SignUP form that we don’t want. Specifically… the way that the signup form is working right now allows people to sign
                    up for an account with a username OR email that someone already has, but then a new account is not created for them on the backend. (If someone has already used the username or email, a new account is not created for them, but they’re not given an error message that the new account was not created on the frontend). Let my teammates know so we can problem-solve how to fix.
                -	Kept working on README datamodel.md, testing features as a front end user, then checking the background data behavior in Beekeeper.
                -	Finished our group's outline for the main files in docs and my individual contributions for apis.md and datamodel.md.
                -	Started on ghi.md and integrations.md in docs folder.
                -	Merged branch 26-readme-individual-contributions to main during screenshare with my teammates after getting approval. Made notes in those files where we need their additions/editing.
                -   Updated Trello board with cards assigned to me and some group cards.
                -   Reviewed unit tests that I wrote and merged to main yesterday; started work on some additional unit tests.

+ Jan 31 Wed    - Finalized unit test, made sure they’re passing for Get Pets and Update Pet fastAPI endpoints
                -	Finished writing up merge request in Gitlab for unit tests
                -	Screenshared merge request and got approval from group to merge my unit test feature branch to main. Merged to main.
                -	Discussed CSS styling with team; voted on some graphics that Chaos and Everest came up with
                -	Set Gitlab project settings back to “Public” per Rosheen’s instructions in Slack (to prepare project for deployment)
                -   Started working on my portions (endpoints, React components) of the README.md file and docs folder.
                -   Reviewed Rosheen's lecture on deployment to prepare for back- and front-end deployment with team next week.
                -   Created checklist (based on Learn module, D1: CI/CD) in editable Word doc for our team to follow next week when doing deployment. "To dos" are highlighted in yellow.

+ Jan 30 Tues - Created issue and corresponding merge request and branch for Get One Pet fastAPI endpoint.
              - Wrote code in routers/pets.py and queries/pets.py to make Get One Pet fastAPI endpoint work.
              - Tested Get One Pet fastAPI endpoint on localhost8000/docs to make sure working as expected (with authentication required).
              - Showed teammates functionality of Get One Pet fastAPI endpoint and they approved my request to merge those changes to main.
              - Finished work on code for frontend React component, Edit Pet. This required code additions to App.js, MyProfile.js
              and
              - Users can now edit a pet's name and image url.
              - Showed teammates functionality of Edit Pet form on the frontend and they approved my request to merge those changes to main.
              •	Resumed work on unit tests. Created test_pets.py file in tests folder. Created unit test for the get all pets endpoint. After an hour or so of troubleshooting and writing code, was able to show that the unit test passes! (Had to figure out how to mock the user authentication data since get pets is a protected endpoint). Now code is ready to merge with main, as the unit test is passing!
              •	Just for my own practice (and because another team member might want to write the unit test for that endpoint), started writing another unit test, this time for the update pet (PUT) fastAPI endpoint. Also going to write that test in the same file, test_pets.py. Still in the 13-unit-test-for-contact-us branch.

+ Jan 29 Mon - Created issue and corresponding merge request and branch for unit test. Branch name:
            13-unit-test-for-contact-us.
             - Created tests folder in api directory for unit tests.
             - Created test_contact_us.py to start playing around with unit tests.
             - Ran "python -m pytest" in Exec tab of fastAPI Docker container to test that unit tests are being collected and passing (2 simple unit tests from test_contact_us.py).
             Tests are functioning as expected (2 passed).
             - Wrote code for unit test of get all contact messages endpoint in file, test_contact_us.py.
             - Ran "python -m pytest" in Exec tab of fastAPI Docker container, to run unit test.
             - Error came back was "ModuleNotFoundError: httpx".
             - Added httpx to requirements.txt, then deleted all docker containers, images, and volumes. Then recreated
             zodb volume, docker-compose build, docker-compose up. Resolved the error.
             - Reran "python -m pytest" with current code. After a couple of trials of adjusting the Assert statement, I
             now have the unit test for get all contact messages fastAPI endpoint passing.
             - Added, committed, and pushed changes to remote branch 13-unit-test-for-contact-us.

## Week 15
+ Jan 26 Fri -	Created issues and corresponding merge requests and branches for 2 frontend React components: Edit Pet form and Contact Us form.
             -	Pulled from main to get latest changes onto Contact Us form branch. Wrote and tested full code to get Contact Us form up and working on the frontend. This included troubleshooting how to make sure that an email address entered into the form is validated as an email address, ensuring that the state of each field is updated within the React Components developer tools as the user types into the form, and that upon successful submission, the user sees a brief message that says “Thank you! Your message has been successfully sent” before then being redirected to the Zoscope homepage. Also wrote in error handling code to handle and display to users that the form was not successfully sent. Pushed all changes of current working Contact Us form up to gitlab. Finished writing the gitlab issue. Working on the content of the Merge Request. Will plan to merge the feature branch with main during a screenshare with my team on Monday. Spent a decent amount of time on this feature making sure that the POST request to the backend endpoint was working and testing (with successful results) that a successful submission from the front end actually does, in fact, create a new row for the message in our database tables (did this testing in Beekeeper). For the Contact Form display, I also worked on making the Message form field bigger; just more visually appealing for the user, hopefully encouraging them to type a message as long as they want. To do this, I used a “textarea” vs. an ”input” field.
            -	Worked on Edit Pet form (frontend React component).
            - Created new branch for journal updates and merge request to update them in Gitlab. Will plan to merge to main on Monday.
            = Added and committed changes to 12-journal-update branch.

+ Jan 25 Thu -
            -	Merge requests made: 2
            -	each with screensharing with team, showed them functionality that I built and how it's working. Each merge request had approvers.
            -	1 merge request was for POST endpoint (Create contactmessage fastAPI endpoint)
            -	1 merge request was for GET endpoint (Get contactmessages fastAPI endpoint)
            -	2 completed merge requests, now merged to main
            -	Created new issue in GitLab for Edit Pet form (#10)
            -	Created merge request and branch for issue. Left merge request as draft.
            -	Pulled from main to begin work in the new branch on my local computer, ‘10-edit-pet-react’

+ Jan 24 Wed - Created a new branch off of main for the new feature that I’m working on, the Contact Us fastAPI endpoint. Named new branch “ContactUs-fastAPI-branch” off of main.
             = Fixed "incompatible migration history" error, which came from pulling latest from main. Fixed by deleting all volumes, images, and containers in Docker, then recreating zodb volume, docker-compose build, docker-compose up.

+ Jan 23 Tues - Completed journal entry for today and pushed to jane-branch.
- Finished writing and testing code to create the Edit/Update Pet fastAPI endpoint. - Created first Issue in GitLab, Issue #3, “Resolve Update/Edit Pet fastAPI endpoint”= Created first merge request connected to Issue in GitLab. Finished documentation in body of merge request in accordance with the MR checklist that Rosheen posted today in Slack. Added Egeman as Approver. Walked through all steps of merge request with team on screenshare. Completed merge request, thus closing Issue #3 as well. Met following Trello checkpoint for this week with this merge request completion: https://trello.com/c/NIoI9xBv
- Created new Trello cards for backend endpoints for individual team members based on checkpoints that Rosheen posted to Slack.

+ Jan 22 Mon - Continued reviewing code for authentication, including backend POST (create account) and backend login logout functionality. Did some testing on my own with creating accounts using the fastAPI endpoint, logging in, and logging out. Also successfully used Beekeeper to run SQL queries on the users database as I created them.- Backend authentication appears to be working mostly as expected. Two things that I found in my testing and want to note for discussion with my group: 1- on some of the fastAPI endpoints, we have the option to input some parameters that don't think we need (we want it to say "No parameters" on those instead of these queries for session_getter and token) and 2 - when logging in on the endpoint, the input label says "username", but really it should say "email" because if for login, I put in the username of a user that I created (example: harrypotter) vs. their email (example: harrypotter@gmail.com), then Execute, I get an Error returned. If I use their email in that username field, we get a successful response. - Tested protected API endpoint.

## Week 14

+ Jan 21 Sun - Met with group on Zoom to review the progress made on authentication. Egeman created merge   request of his work and as a group, we walked through, troubleshooted pipeline issue, and were able to complete the merge request.- Pulled changes from main (reflecting updated authentication). Merged with local branch, jane-branch, and checked out to jane-branch to continue working with new changes from Egeman.- Following pull from main, deleted all Docker volumes, containers, and images. Ran the following Docker commands to rebuild containers after the pull from main: 1 - docker volume create zodb, 2= docker-compose build, 3- docker-compose up

+ Jan 20 Sat - Created ContactUs.js in VS code. Started writing code for the Contact Us form.
             - Created EditPet.js in VS code. Started writing code for the Edit Pet form.

+ Jan 19 Fri - Continued working on our blocker, the inability to create an account.
             - Went through several different errors today in trying to configure backend authentication. The most time-consuming error to troubleshoot was a Type error "Object of type date is not JSON serializable" that was occurring upon trying to create an account using fastAPI. We were trying to send in a birthday with a user when we create an account, as dictated by our design. Eventual solution that we had to come to, because we could not solve another way, is that we're going to keep the create account data as close to Curtis' jwt token documentation as possible, only sending username, email, and password. Then we're going to create a separate table, "User Info", that has a Foreign Key relationship to the User table (with the Curtis stuff), that can contain birthday and any other user-associated information that's not critical to the authentication piece. Then the plan is to make two endpoint calls when we create an account. We'll create the account and also have to take the birthdays and other info and send that to another endpoint that will create the account details. (FYI, the error that we continued to get was that a str type was not expected for birthday, even though when we print out type(birthday), it returns string. So it is a string, but the console log is somehow telling us that it is not a string and thus cannot create the account).
             - Started draft of our README.md to later present to group and discuss.

+ Jan 18 Thu - Installed BeeKeeper and set up postgres connection for managing tables in project.
             - Used jwt-fastAPI docs to set up authentication with team (mob programming)
             - Worked on create account (POST) endpoint with team; spent time troubleshooting a series of different errors that were preventing us from creating a user. Still stuck on why we are returning "None" for our table rows when we seem to have been able to create an account. Will need to continue to tackle this tomorrow as this is now a blocker issue.

+ Jan 17 Wed - Added this week's checkpoints from Rosheen to group's Trello for project tracking.
             - Worked with group on setting up our postrgres database and pushing that code to main.
             - Worked with group oin general on coordinating our "git as a group" process (e.g., how to create and approve merge requests, push/pull to and from our dev branches to main).
             - Participated in first group standup and updated group on my work thus far.
             - Honestly spent lots of time today having to troubleshoot a gitlab issue (pulling changes from main that weren't being recognized by my dev branch). Sought help from my group members and SEIR with this, and we decided that the best path forward was for me to just scrap my dev branch and create a new one. Created new dev branch, jane-branch, and pulled changes from main.
             - Rebuilt docker containers and create new docker volume for database, zodb.

+ Jan 16 Tue - Continued working on wire frame and API endpoint design of overall project.
             - Presented our wire frame and API endpoint design as a group to Rosheen.
             - Took notes during presentation on feedback and sent out doc file to group re: next steps.
             - Followed steps for setting up database on my own in preparation for doing as a group tomorrow.

## Week 13

+ Jan 12 Fri - Worked on finalizing the wire frame and API endpoint design with team.
             - Added details on endpoint design to our team's shared google doc.
             - Updated Trello cards to reflect progress we made this week and next steps.

+ Jan 11 Thu - Continued editing/ fine-tuning our wire frame in prep for presentation and feedback next week.
             - Started writing some code for the endpoints that I was assigned (Contact Us and Edit Pet).
             - Updated Trello board with individual responsibilities and weekly checkpoints.

+ Jan 10 Wed - Followed instructions to get Docker containers up and running.
             - Continued working on API endpoint design and delegating responsibilities amongst teammates.

+ Jan 9 Tue - Narrowed down our list of ideas for the project.
            - Selected one idea through series of group votes and discussion.
            - Began wire framing our idea for the project with the group.
            = Created new group on Gitlab, The Terrible Twos. Added necessary staff to project.
            - Forked repo in GitLab and named our project, "ZoScope".
            - Cloned ZoScope repo to local computer.
            - Created development branch, sundermann-branch, to study and manipulate code on my own.

+ Jan 8 Mon - Decided to use Trello as a group for project management moving forward.
            - Created some initial Trello cards to organize our planning/ project creation stage.
            - Discussed project ideas with group, including pros and cons of each idea. Talked about scope and made sure that our ideas were lining up with project requirements.
