# Integrations

The application will need to get the following kinds of data from third-party sources:

- SendGrid API
- OpenAI API
- Lunar calender data from Rapid API
