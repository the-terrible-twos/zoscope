# Data models

---

### Accounts


| name             | type   | unique | optional |
| ---------------- | ------ | ------ | -------- |
| id               | int    | yes    | no       |
| username         | string | yes    | no       |
| email            | string | no     | no       |
| first_name       | string | no     | no       |
| last_name        | string | no     | no       |
| birthday         | date   | no     | no       |
| starsign         | string | no     | no       |


The `account` entity contains the data about the Zoscope account holder.

---

### Pets

| name             | type   | unique | optional               |
| ---------------- | ------ | ------ | -----------------------|
| id               | int    | yes    | no                     |
| name             | string | no     | no                     |
| type             | string | no     | yes                    |
| birthday         | date   | no     | yes, if adopted = True |
| adopted          | boolean| no     | yes                    |
| starsign         | string | no     | no                     |
| image            | string | no     | yes                    |


The `pet` entity contains the data about an individual pet.

---

### Horoscopes

| name             | type   | unique | optional               |
| ---------------- | ------ | ------ | -----------------------|
| id               | int    | yes    | no                     |
| pet_id           | string | yes    | no                     |
| created_on       | date   | no     | no                     |
| fortune          | string | yes    | no                     |


The `horoscope` entity contains the data about a horoscope.

---

### Compatibilities

| name    | type   | unique | optional |
| ------- | ------ | ------ | -------- |
| id      | int    | yes    | no       |
| user_id | int    | yes    | no       |
| pet_id  | int    | yes    | no       |
| message | string | yes    | no       |

The `compatibility` entity contains the data about a compatibility.

---

### Starsigns

| name             | type   | unique | optional               |
| ---------------- | ------ | ------ | -----------------------|
| id               | int    | yes    | no                     |
| sign_name        | string | no     | no                     |
| start_date       | date   | no     | no                     |
| end_date         | date   | no     | no                     |


The `starsign` entity contains the data about a starsigns.

---

### Gen Horoscopes

| name             | type   | unique | optional               |
| ---------------- | ------ | ------ | -----------------------|
| id               | int    | yes    | no                     |
| pet_name         | string | no     | no                     |
| birthday         | date   | no     | no                     |
| adopted          | bool   | no     | yes                    |
| created_on       | date   | no     | no                     |
| fortune          | string | no     | no                     |

The `gen_horoscope` entity contains the data about non registered user's horoscopes.
