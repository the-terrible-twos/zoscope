# APIs

## Accounts

**Method**: 'POST', 'DELETE'
**Path**: /token, /accounts

Create Account:
/accounts
POST Input:

```json
{
  "username": "WideIan",
  "email": "WideIan@WideAlex.com",
  "password": "WideAlex",
  "first_name": "Wide",
  "last_name": "Ian",
  "birthday": "2023-10-09"
}
```

Login:
/token
POST Input:

```json
{
  "username": "WideIan",
  "password": "WideAlex"
}
```

POST Output:

```json
{
  "access_token": "string",
  "token_type": "Bearer"
}
```

Logout:
/token
DELETE output:

```json
{
  true
}
```

## Horoscopes

- **Method**: 'POST', 'GET', 'DELETE'
- **Path**: '/horoscopes, /horoscopes/{id}

POST Input:

```json
{
  "pet_id": 1,
  "created_on": "2024-02-05"
}
```

POST Output:

```json
{
  "id": 1,
  "pet_id": 1,
  "fortune": "insert generated fortune"
}
```

The get function wasn't implemented in time for submission, but ostensibly a registered user should be able to
get any of the horoscopes they've previously generated for a pet. The backend works, it simply doesn't exist for user engagement.

GET Input:

```json
{
  "id": 0,
  "pet_id": 0,
  "fortune": "string"
}
```

GET Output:

```json
{
  "id": 0,
  "pet_id": 0,
  "fortune": "string"
}
```

Same here, a user could choose to delete a specific horoscope if they wanted to. It works in the backend,
it just wasn't implemented into the site.

DELETE Input:

```json
{
  "id": 0
}
```

Delete Output:

```json
{
  "true"
}
```

## Gen Horoscopes

- **Method**: 'POST'
- **Path**: '/gen_horoscopes'

POST Input:

```json
{
  "pet_name": "string",
  "birthday": "2024-02-09",
  "type": "string",
  "adopted": true
}
```

POST Output:

```json
{
  "id": 0,
  "pet_name": "string",
  "birthday": "2024-02-09",
  "type": "string",
  "adopted": true,
  "created_on": "2024-02-09",
  "fortune": "string"
}
```

## Pets

- **Method**: 'POST', 'GET', 'GET', 'PUT', 'DELETE',
- **Path**: `/pets, `/pets/{pet_id}`

POST Input:

```json
{
  "name": "Loki",
  "type": "dog",
  "birthday": "2019-03-05",
  "adopted": false,
  "image": "https://www.shutterstock.com/image-photo/funny-dog-licking-lips-tongue-260nw-1761385949.jpg"
}
```

POST Output:

```json
{
  "id": 2,
  "user_id": 2,
  "name": "Loki",
  "type": "Dog",
  "birthday": "2019-03-05",
  "adopted": false,
  "starsign": "N/A",
  "image": "https://www.shutterstock.com/image-photo/funny-dog-licking-lips-tongue-260nw-1761385949.jpg"
}
```

PUT Input:

Enter the pet_id (an integer) as a Parameter. In this example, we are updating a pet with a pet_id of 2, so
we enter 2 in the pet_id Parameter field.
In the JSON request body, we change the value of name and/or image to the desired value.
Click Execute to update the pet's details to the values shown below.

```json
{
  "name": "Loki Jr.",
  "image": "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg"
}
```

PUT Output:

```json
{
  "name": "Loki Jr.",
  "image": "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg"
}
```

GET Input:

Method can list users' pets. In this example, we are listing pets belong to the user with id number 1.

In the JSON request body, we need to log in to the user account.
The user ID will be determined automatically.
Click Execute to list the pets that belong the user.

GET Output:

```json
[
  {
    "id": 1,
    "user_id": 1,
    "name": "Ocean",
    "type": "Dog",
    "birthday": "2020-03-10",
    "adopted": false,
    "starsign": "Pisces",
    "image": "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg"
  },
  {
    "id": 2,
    "user_id": 1,
    "name": "Olive",
    "type": "Cat",
    "birthday": "2022-10-15",
    "adopted": true,
    "starsign": "Libra",
    "image": "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg"
  }
]
```

GET Input:

Enter the pet_id (an integer) as a Parameter. In this example, we are getting a pet with a pet_id of 1, so
we enter 1 in the pet_id Parameter field.
In the JSON request body, we need to log in to the user account.
The user ID will be determined automatically.
Click Execute to get the pet's details to the values shown below.

GET Output:

```json
{
  "id": 1,
  "user_id": 1,
  "name": "Ocean",
  "type": "Dog",
  "birthday": "2020-03-10",
  "adopted": false,
  "starsign": "Pisces",
  "image": "https://images.pexels.com/photos/1108099/pexels-photo-1108099.jpeg"
}
```

## Compatibilities

- **Method**: 'POST'
- **Path**: `/compatibilities`

POST Input:

```json
{
  "pet_id": 1,
  "user_id": 1
}
```

POST Output:

```json
{
  "id": 1,
  "pet_id": 1,
  "user_id": 1,
  "message": "string"
}
```

## Lunar Calendar

- **Method**: 'GET'
- **Path**: `/lunarcalendar

GET Input:

Method displays lunar calendar on page for user

In the JSON request body, the user navigates to Lunar Calendar page and a request is made to url: "https://moon-phase.p.rapidapi.com/calendar?format=html"
Header: 'X-RapidAPI-Key': "REACT_APP_MOON_API_KEY"
'X-RapidAPI-Host': "moon-phase.p.rapidapi.com"

GET Output:

```json
{
  "type": "string"
}
```

Delete Input:

This endpoint is responsible for deleting a pet from the database. It takes the pet ID
as a parameter. For this example, we enter 1 as the pet ID. If the deletion is successful
you will get a status code of 200 and a value of true.

Delete Output:

```json
{
  true
}
```

## Email

**Method**: 'POST'
**Path**: /email/send_email

POST Input:

```json
{
  "from_email": "123@gmail.com",
  "name": "WideIan",
  "subject": "Cool Website",
  "content": "You made such a cool website!!"
}
```

POST Output:

```json
{
  "string"
}
```
