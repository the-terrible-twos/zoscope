steps = [
    [
        """
        CREATE TABLE horoscopes (
            id SERIAL PRIMARY KEY NOT NULL,
            pet_id INT NULL REFERENCES pets (id) ON DELETE CASCADE,
            created_on DATE,
            fortune TEXT
        );
        """,

        """
        DROP TABLE horoscopes;
        """
    ]
]
