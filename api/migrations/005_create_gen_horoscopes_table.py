steps = [
    [
        """
        CREATE TABLE gen_horoscopes (
            id SERIAL PRIMARY KEY NOT NULL,
            pet_name VARCHAR(40) NOT NULL,
            birthday DATE NOT NULL,
            type VARCHAR(20) NOT NULL,
            adopted BOOLEAN,
            created_on DATE,
            fortune TEXT
        );
        """,

        """
        DROP TABLE horoscopes;
        """
    ]
]
