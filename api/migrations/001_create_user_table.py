steps = [
    [
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(20) NOT NULL UNIQUE,
            email VARCHAR(30) NOT NULL UNIQUE,
            password VARCHAR(500) NOT NULL,
            first_name VARCHAR(20) NOT NULL,
            last_name VARCHAR(20) NOT NULL,
            birthday DATE NOT NULL,
            starsign VARCHAR(20)
        );
        """,
        """
        DROP TABLE users;
        """
    ]
]
