steps = [
    [
        """
        CREATE TABLE starsigns (
            id SERIAL PRIMARY KEY NOT NULL,
            sign_name VARCHAR(20) NOT NULL,
            start_date DATE NOT NULL,
            end_date DATE NOT NULL
        );
        """,
        """
        DROP TABLE starsigns;
        """
    ]
]
