steps = [
    [
        """
        CREATE TABLE pets (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL REFERENCES users (id) ON DELETE CASCADE,
            name VARCHAR(20) NOT NULL,
            type VARCHAR(20) NOT NULL,
            birthday DATE,
            adopted BOOLEAN,
            starsign VARCHAR(20),
            image VARCHAR(400) NOT NULL DEFAULT 'https://sagemailer.com/blog/wp-content/uploads/2020/08/Amazon-Pet-Profile-Why-You-Should-Sign-Up.jpg'
        );
        """,

        """
        DROP TABLE pets;
        """
    ]
]
