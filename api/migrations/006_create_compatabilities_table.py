steps = [
    [
        """
        CREATE TABLE compatibilities (
            id SERIAL PRIMARY KEY NOT NULL,
            user_id INT NOT NULL REFERENCES users (id) ON DELETE CASCADE,
            pet_id INT NOT NULL REFERENCES pets (id) ON DELETE CASCADE,
            message TEXT
        );
        """,


        """
        DROP TABLE compatibilities;
        """
    ]
]
