from fastapi import FastAPI
from authenticator import authenticator
from routers import accounts, pets, horoscopes
from routers import gen_horoscopes, email, compatibilities
from fastapi.middleware.cors import CORSMiddleware
import os

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(pets.router)
app.include_router(horoscopes.router)
app.include_router(gen_horoscopes.router)
app.include_router(email.router)
app.include_router(compatibilities.router)

CORS_HOST = os.environ["CORS_HOST"]
print(CORS_HOST)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        CORS_HOST
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }


@app.get("/")
def home_page():
    return {
        "message": "Welcome to Zooscope!"
    }
