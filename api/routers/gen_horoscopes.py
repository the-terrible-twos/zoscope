from fastapi import APIRouter, Depends, Response
from typing import Union
from queries.gen_horoscopes import (
    GenHoroscopeIn,
    GenHoroscopeRepository,
    GenHoroscopeOut,
    Error,
)


router = APIRouter()


@router.post("/gen_horoscopes", response_model=Union[GenHoroscopeOut, Error])
def create_gen_horoscope(
    gen_horoscope: GenHoroscopeIn,
    response: Response,
    repo: GenHoroscopeRepository = Depends(),
):
    try:
        return repo.create(gen_horoscope)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not create horoscope"}
