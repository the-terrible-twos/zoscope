import os
from fastapi import APIRouter
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from pydantic import BaseModel


router = APIRouter()


class EmailTemplate(BaseModel):
    from_email: str
    name: str
    subject: str
    content: str


@router.post("/email/send_email/")
async def send_email(email: EmailTemplate):
    msg = Mail(
        from_email="Zoscope3@gmail.com",
        to_emails="Zoscope2@gmail.com",
        subject=email.name + " - " + email.from_email + " - " + email.subject,
        plain_text_content=email.content
    )

    try:
        sg = SendGridAPIClient(os.environ.get("SENDGRID_API_KEY"))
        sg.send(msg)
        return {"message": "Email sent successfully"}
    except Exception as e:
        return {"message": f"Failed to send email: {e}"}
