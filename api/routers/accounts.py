from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from typing import Union, List
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from pydantic import BaseModel
from queries.accounts import (
    AccountIn,
    AccountOut,
    AccountQueries,
    DuplicateAccountError,
    Error,
)


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


async def get_current_user(
    token: str = Depends(Token),
    repo: AccountQueries = Depends(),
) -> Union[AccountOut, str]:
    try:
        payload = authenticator.decode_token(token)
        username: str = payload.get("sub")
        if username is None:
            return ("username is none")
    except Exception as e:
        print(e)
        return ("error")

    user = repo.get(username)

    if user is None:
        return ("could not retrieve user data")

    else:
        return user

router = APIRouter()


@router.get("/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.post("/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    try:
        hashed_password = authenticator.hash_password(info.password)
        try:
            account = accounts.create(info, hashed_password)
        except DuplicateAccountError:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Cannot create an account with those credentials",
            )
        form = AccountForm(username=info.username, password=info.password)
        token = await authenticator.login(response, request, form, accounts)
        return AccountToken(account=account, **token.dict())
    except Exception as e:
        print(e)


@router.get("/accounts", response_model=Union[List[AccountOut], Error])
async def get_all_accounts(accounts: AccountQueries = Depends()):
    all_accounts = accounts.get_all()
    return all_accounts


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data)
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }
