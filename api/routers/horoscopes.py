from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from typing import Union, List
from queries.horoscopes import (
    HoroscopeIn,
    HoroscopeRepository,
    HoroscopeOut,
    Error,
)


router = APIRouter()


@router.post("/horoscopes", response_model=Union[HoroscopeOut, Error])
def create_horoscope(
    horoscope: HoroscopeIn,
    response: Response,
    repo: HoroscopeRepository = Depends(),
    h_data: dict = Depends(authenticator.get_current_account_data)
):
    try:
        return repo.create(horoscope)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not create horoscope"}


@router.delete("/horoscopes/{id}", response_model=bool)
def delete_horoscope(
    id: int,
    response: Response,
    repo: HoroscopeRepository = Depends(),
) -> bool:
    try:
        return repo.delete(id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete horoscope"}


@router.get("/horoscopes", response_model=Union[List[HoroscopeOut], Error])
def get_horoscopes(
    response: Response,
    repo: HoroscopeRepository = Depends(),
    h_data: dict = Depends(authenticator.get_current_account_data)
):
    try:
        horoscopes = repo.get_horoscopes(h_data["id"])
        return horoscopes
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get horoscopes of pet"}
