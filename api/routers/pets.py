from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from typing import Union, List, Optional
from queries.pets import (
    PetIn,
    PetUpdateIn,
    PetUpdateOut,
    PetRepository,
    PetOut,
    Error,
)


router = APIRouter()


@router.post("/pets", response_model=Union[PetOut, Error])
def create_pet(
    pet: PetIn,
    response: Response,
    repo: PetRepository = Depends(),
    pet_data: dict = Depends(authenticator.get_current_account_data)
):
    try:
        return repo.create(pet, pet_data["id"])
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not create pet"}


@router.delete("/pets/{pet_id}", response_model=bool)
def delete_pet(
    pet_id: int,
    response: Response,
    repo: PetRepository = Depends(),
) -> bool:
    try:
        return repo.delete(pet_id)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not delete pet"}


@router.get("/pets", response_model=Union[List[PetOut], Error])
def get_pets(
    response: Response,
    repo: PetRepository = Depends(),
    pet_data: dict = Depends(authenticator.get_current_account_data)
):
    try:
        pets = repo.get_pets(pet_data["id"])
        return pets
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not get pets of user"}


@router.put("/pets/{pet_id}", response_model=Union[PetUpdateOut, Error])
def update_pet(
    pet_id: int,
    pet: PetUpdateIn,
    response: Response,
    repo: PetRepository = Depends(),
    pet_data: dict = Depends(authenticator.get_current_account_data)
) -> Union[Error, PetUpdateOut]:
    try:
        return repo.update(pet_id, pet)
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not update pet"}


@router.get("/pets/{pet_id}", response_model=Optional[PetOut])
def get_one_pet(
    pet_id: int,
    response: Response,
    repo: PetRepository = Depends(),
    pet_data: dict = Depends(authenticator.get_current_account_data)
) -> PetOut:
    pet = repo.get_one(pet_id)
    if pet is None:
        response.status_code = 404
    return pet
