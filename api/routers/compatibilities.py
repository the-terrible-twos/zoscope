from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from typing import Union
from queries.compatibilities import (
    CompatibilityIn,
    CompatibilityOut,
    CompatibilityRepo,
    Error,
)


router = APIRouter()


@router.post("/compatibilities", response_model=Union[CompatibilityOut, Error])
def create_compatibility(
    compatibility: CompatibilityIn,
    response: Response,
    repo: CompatibilityRepo = Depends(),
    authenticated_user_data: dict = Depends(authenticator.get_current_account_data)  # noqa: E501
        ):

    try:
        print("authenticated_user_data:", authenticated_user_data)
        if authenticated_user_data:
            return repo.create(compatibility)
        else:
            response.status_code = 401
            return {"message": "Authentication failed"}
    except Exception as e:
        print(e)
        response.status_code = 400
        return {"message": "Could not create compatibility report"}
