from pydantic import BaseModel
from typing import List, Union
from datetime import date
from queries.pool import pool

from typing import Optional


class Error(BaseModel):
    message: str


class PetIn(BaseModel):
    name: str
    type: str
    birthday: date
    adopted: bool
    image: str


class PetUpdateIn(BaseModel):
    name: str
    image: str


class PetOut(BaseModel):
    id: int
    user_id: int
    name: str
    type: str
    birthday: date
    adopted: bool
    starsign: Optional[str]
    image: str


class PetUpdateOut(BaseModel):
    name: str
    image: str


class PetRepository:
    def get_one(self, pet_id: int) -> Optional[PetOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , user_id
                            , name
                            , type
                            , birthday
                            , adopted
                            , starsign
                            , image
                        FROM pets
                        WHERE id = %s
                        """,
                        [pet_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_pet_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that pet"}

    def update(self,
               pet_id: int,
               pet: PetUpdateIn) -> Union[PetUpdateOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE pets
                        SET name = %s
                        , image = %s
                        WHERE id = %s
                        """,
                        [
                            pet.name,
                            pet.image,
                            pet_id
                        ]
                    )
                    return self.pet_in_to_out(pet_id, pet)
        except Exception as e:
            print(e)
            return {"message": "Could not update that pet"}

    def create(self,
               pet: PetIn,
               user_id: int) -> PetOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    INSERT INTO pets(user_id, name, type,
                    birthday, adopted, image)
                    VALUES(%s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        user_id,
                        pet.name,
                        pet.type,
                        pet.birthday,
                        pet.adopted,
                        pet.image,
                    ]
                )
                data = db.fetchone()
                id = data[0]

                db.execute(
                    """
                    UPDATE pets
                    SET starsign = starsigns.sign_name
                    FROM starsigns
                    WHERE pets.user_id = %s
                    AND (EXTRACT (MONTH FROM pets.birthday) = EXTRACT (MONTH FROM starsigns.start_date)
                    OR EXTRACT (MONTH FROM pets.birthday) = EXTRACT (MONTH FROM starsigns.end_date))
                    AND (EXTRACT (DAY FROM pets.birthday) >= EXTRACT (DAY FROM starsigns.start_date)
                    OR EXTRACT (DAY FROM pets.birthday) <= EXTRACT (DAY FROM starsigns.end_date))
                    """,   # noqa: E501
                    [user_id]
                )

                db.execute(
                    """
                    SELECT starsign
                    FROM pets
                    WHERE id = %s
                    """,
                    [id],
                )

                starsign_data = db.fetchone()
                starsign = starsign_data[0] if starsign_data else None

                if id is None:
                    return None

                return PetOut(
                    id=id,
                    user_id=user_id,
                    name=pet.name,
                    type=pet.type,
                    birthday=str(pet.birthday),
                    adopted=pet.adopted,
                    starsign=starsign or "N/A",
                    image=pet.image
                )

    def get_pets(
            self,
            user_id: int) -> Union[List[PetOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, user_id, name, type,
                        birthday, adopted, starsign, image
                        FROM pets
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id]
                    )

                    return [
                        PetOut(
                            id=record[0],
                            user_id=record[1],
                            name=record[2],
                            type=record[3],
                            birthday=record[4],
                            adopted=record[5],
                            starsign=record[6],
                            image=record[7]
                        )
                        for record in db
                    ]

        except Exception as e:
            print(e)
            return {"message": "Could not get pets of user!!!"}

    def delete(self, pet_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM pets
                        WHERE id = %s
                        """,
                        [pet_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def pet_in_to_out(self, id: int, pet: PetUpdateIn) -> PetUpdateOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT user_id, starsign
                    FROM pets
                    WHERE id = %s
                    """,
                    [id],
                )
                data = db.fetchone()
                if data:
                    user_id, starsign = data
                    old_data = pet.dict()
                    return PetUpdateOut(id=id,
                                        user_id=user_id,
                                        starsign=starsign,
                                        **old_data)
                else:
                    raise ValueError("Pet not found or missing information")

    def record_to_pet_out(self, record):
        return PetOut(
            id=record[0],
            user_id=record[1],
            name=record[2],
            type=record[3],
            birthday=record[4],
            adopted=record[5],
            starsign=record[6],
            image=record[7],
        )
