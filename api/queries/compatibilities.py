import os
from openai import OpenAI
from pydantic import BaseModel
from queries.pool import pool


class Error(BaseModel):
    message: str


class CompatibilityIn(BaseModel):
    pet_id: int
    user_id: int


class CompatibilityOut(BaseModel):
    id: int
    pet_id: int
    user_id: int
    message: str


class CompatibilityRepo:
    def create_message(self, user_pet_details: str) -> str:
        client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))
        response = client.completions.create(
            model="gpt-3.5-turbo-instruct",
            prompt=(
                    "Create a fun silly message about the compatibility of" +
                    " an owner and their pet based on their starsigns," +
                    " as well as their compatibility" +
                    f" percentage and starsigns : {user_pet_details}"
                    ),
            max_tokens=600
        )
        return response.choices[0].text.strip()

    def create(
            self,
            compatibility: CompatibilityIn,
            ) -> CompatibilityOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT starsign
                    FROM pets
                    WHERE id = %s;
                    """,
                    [
                        compatibility.pet_id
                    ]
                )
                pet_starsign = db.fetchone()
                if not pet_starsign:
                    return Error("No starsign found for pet")

                db.execute(
                    """
                    SELECT starsign
                    FROM users
                    WHERE id = %s;
                    """,
                    [
                        compatibility.user_id
                    ]
                )
                user_starsign = db.fetchone()
                if not user_starsign:
                    return Error("No starsign found for user")

                user_pet_details = (
                    f"Human starsign: {user_starsign}" +
                    f"Pet starsign: {pet_starsign}"
                    )
                message = self.create_message(user_pet_details)

                db.execute(
                    """
                    INSERT INTO compatibilities(
                    user_id, pet_id, message)
                    VALUES (%s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        compatibility.user_id,
                        compatibility.pet_id,
                        message
                    ]
                )

                id = db.fetchone()[0]
                conn.commit()

                return CompatibilityOut(
                    id=id,
                    user_id=compatibility.user_id,
                    pet_id=compatibility.pet_id,
                    message=message
                    )
