import os
from openai import OpenAI
from datetime import datetime, date
from pydantic import BaseModel
from queries.pool import pool


class Error(BaseModel):
    message: str


class GenHoroscopeIn(BaseModel):
    pet_name: str
    birthday: date
    type: str
    adopted: bool


class GenHoroscopeOut(BaseModel):
    id: int
    pet_name: str
    birthday: date
    type: str
    adopted: bool
    created_on: date
    fortune: str


class GenHoroscopeRepository:

    def generate_fortune(self, pet_details_str: str) -> str:
        client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))
        response = client.completions.create(
            model="gpt-3.5-turbo-instruct",
            prompt=(
                "Generate a quirky, tongue-in-cheek horoscope" +
                "written in the voice of the associated pet" +
                "(i.e. written for the animal, by the animal)" +
                f"using the following details: {pet_details_str}"
                ),
            max_tokens=600
        )
        return response.choices[0].text.strip()

    def create(
            self,
            horoscope: GenHoroscopeIn,
            ) -> GenHoroscopeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:

                created_on = datetime.now().date()

                pet_details_str = (
                    f"Name: {horoscope.pet_name}, " +
                    f"Birthday: {horoscope.birthday}, " +
                    f"Type: {horoscope.type}"
                    )
                fortune = self.generate_fortune(pet_details_str)

                db.execute(
                    """
                    INSERT INTO gen_horoscopes(
                    pet_name, birthday, type, adopted,
                    created_on, fortune)
                    VALUES (%s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        horoscope.pet_name,
                        horoscope.birthday,
                        horoscope.type,
                        horoscope.adopted,
                        created_on,
                        fortune
                    ]
                )

                id = db.fetchone()[0]
                conn.commit()

                return GenHoroscopeOut(
                    id=id,
                    pet_name=horoscope.pet_name,
                    birthday=horoscope.birthday,
                    type=horoscope.type,
                    adopted=horoscope.adopted,
                    created_on=created_on,
                    fortune=fortune
                )
