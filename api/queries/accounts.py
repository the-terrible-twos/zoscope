from pydantic import BaseModel
from .pool import pool
from typing import Union
from datetime import date


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    email: str
    password: str
    first_name: str
    last_name: str
    birthday: date


class AccountOut(BaseModel):
    id: str
    username: str
    email: str
    first_name: str
    last_name: str
    birthday: str
    starsign: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountQueries:

    def get(self, username: str) -> Union[AccountOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, username, email,
                        first_name, last_name, birthday, starsign, password
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    user = db.fetchone()
                    if user is None:
                        return Error(message="Account not found")

                    id, username, email, first_name, last_name, birthday, starsign, hashed_password = user  # noqa: E501

                    return AccountOutWithPassword(
                        id=id,
                        username=username,
                        email=email,
                        hashed_password=hashed_password,
                        first_name=first_name,
                        last_name=last_name,
                        birthday=str(birthday),
                        starsign=starsign or "N/A",
                    )

        except Exception:
            return Error(message="Could not get account")

    def create(
        self,
        user: AccountIn,
        hashed_password: str,
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO users(username, email, password,
                        first_name, last_name, birthday)
                        VALUES(%s, %s, %s, %s, %s, %s)
                        RETURNING id
                        """,
                        [
                            user.username,
                            user.email,
                            hashed_password,
                            user.first_name,
                            user.last_name,
                            user.birthday

                        ],
                    )
                    data = db.fetchone()
                    id = data[0]

                    db.execute(
                        """
                        UPDATE users
                        SET starsign = starsigns.sign_name
                        FROM starsigns
                        WHERE (EXTRACT (MONTH FROM users.birthday) = EXTRACT (MONTH FROM starsigns.start_date)
                            OR EXTRACT (MONTH FROM users.birthday) = EXTRACT (MONTH FROM starsigns.end_date))
                            AND (EXTRACT (DAY FROM users.birthday) >= EXTRACT (DAY FROM starsigns.start_date)
                                OR EXTRACT (DAY FROM users.birthday) <= EXTRACT (DAY FROM starsigns.end_date))
                        """  # noqa: E501
                    )

                    db.execute(
                        """
                        SELECT starsign
                        FROM users
                        WHERE id = %s
                        """,
                        [id],
                    )

                    starsign_data = db.fetchone()
                    starsign = starsign_data[0] if starsign_data else None

                    if id is None:
                        return None

                    return AccountOutWithPassword(
                        id=id,
                        username=user.username,
                        email=user.email,
                        hashed_password=hashed_password,
                        first_name=user.first_name,
                        last_name=user.last_name,
                        birthday=str(user.birthday),
                        starsign=starsign or "N/A",
                    )
        except Exception as e:
            print(f"Error during account creation: {e}")
            if "duplicate key" in str(e):
                raise DuplicateAccountError(
                    "Username or email already exists.")
            else:
                raise e
