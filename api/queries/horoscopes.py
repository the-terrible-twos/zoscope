import os
from openai import OpenAI
from datetime import date
from typing import List, Union
from pydantic import BaseModel
from queries.pool import pool


class Error(BaseModel):
    message: str


class HoroscopeIn(BaseModel):
    pet_id: int
    created_on: date


class HoroscopeOut(BaseModel):
    id: int
    pet_id: int
    fortune: str


class HoroscopeRepository:

    def generate_fortune(self, pet_details_str: str) -> str:
        client = OpenAI(api_key=os.getenv("OPENAI_API_KEY"))
        response = client.completions.create(
            model="gpt-3.5-turbo-instruct",
            prompt=(
                "Generate a quirky, tongue-in-cheek horoscope " +
                "written in the voice of the associated pet " +
                "(i.e. written for the animal, by the animal) " +
                f"using the following details: {pet_details_str}"
                ),
            max_tokens=600
        )
        return response.choices[0].text.strip()

    def create(
            self,
            horoscope: HoroscopeIn,
            ) -> HoroscopeOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT name, type, adopted, starsign
                    FROM pets
                    WHERE id = %s;
                    """,
                    [horoscope.pet_id]
                )

                pet_data = db.fetchone()

                if pet_data:
                    name, type, adopted, starsign = pet_data
                    pet_details_str = (
                        f"Name: {name}, Type: {type}, Starsign: {starsign}" +
                        f" Adopted: {adopted}"
                        )
                    fortune = self.generate_fortune(pet_details_str)

                db.execute(
                    """
                    INSERT INTO horoscopes(
                    pet_id, created_on, fortune)
                    VALUES (%s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        horoscope.pet_id,
                        horoscope.created_on,
                        fortune
                    ]
                )

                id = db.fetchone()[0]
                conn.commit()

                return HoroscopeOut(
                    id=id,
                    pet_id=horoscope.pet_id,
                    fortune=fortune
                )

    def delete_horoscope(self, id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM horoscopes
                        WHERE id = %s
                        """,
                        [id]
                    )
                return True
        except Exception as e:
            print(e)
            return False

    def get_horoscopes(
            self,
            pet_id: int
            ) -> Union[List[HoroscopeOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, pet_id, created_on, fortune
                        FROM horoscopes
                        WHERE pet_id = %s
                        ORDER BY created_on;
                        """,
                        [pet_id]
                    )
                    records = db.fetchall()

                    return [
                        HoroscopeOut(
                            id=record[0],
                            pet_id=record[1],
                            created_on=record[2],
                            fortune=record[3]
                        )
                        for record in records
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get the pet's horoscopes."}
