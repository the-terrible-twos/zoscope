from fastapi.testclient import TestClient
from main import app
from queries.pets import PetRepository

client = TestClient(app)


class FakePetRepository:
    def delete(self, pet_id: int):
        return 1 == pet_id


def test_delete_pet():
    # Arrange
    app.dependency_overrides[PetRepository] = FakePetRepository

    # Act
    response = client.delete("/pets/1")

    # Assert
    assert response.status_code == 200
    assert response.json() is True
    app.dependency_overrides = {}
