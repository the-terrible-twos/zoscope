from fastapi.testclient import TestClient
from main import app
from queries.pets import PetRepository, PetUpdateIn, PetUpdateOut
from authenticator import authenticator


client = TestClient(app)


def test_init():
    assert 1 == 1


def fake_get_current_account_data():
    return {"id": 1, "username": "testuser"}


class FakePetRepository:
    def get_pets(self, user_id: int):
        assert user_id == 1
        return []

    def update(self, pet_id: int, pet: PetUpdateIn):
        assert pet_id == 1
        return PetUpdateOut(name=pet.name, image=pet.image)


def test_get_pets():
    # Arrange
    app.dependency_overrides[PetRepository] = FakePetRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    response = client.get("/pets")

    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200, (
        "Expected status code 200, got {}".format(response.status_code)
    )
    assert response.json() == [], (
        "Expected JSON response to be an empty list"
    )


def test_update_pet():
    # Arrange
    app.dependency_overrides[
        PetRepository
    ] = FakePetRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    pet_id_to_update = 1
    update_data = {
        "name": "Updated Pet Name",
        "image": "updated_image_url"
    }

    # Act
    response = client.put(f"/pets/{pet_id_to_update}", json=update_data)
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200, (
        "Expected status code 200, got {}".format(response.status_code)
    )
    updated_pet = response.json()
    assert updated_pet["name"] == update_data["name"], (
        "Expected pet name to be updated"
    )
    assert updated_pet["image"] == update_data["image"], (
        "Expected pet image to be updated"
    )
