from fastapi.testclient import TestClient
from main import app
from queries.pets import PetRepository, PetIn, PetOut
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": 1, "username": "testuser"}


class CreatePetQueries:
    def create(self, pet: PetIn, user_id: int):
        return PetOut(id=1, user_id=user_id, name=pet.name, type=pet.type,
                      birthday=pet.birthday, adopted=pet.adopted,
                      starsign="N/A", image=pet.image)


class EmptyPetRepository:
    def get_one(self, pet_id: int):
        assert pet_id == 1
        return PetOut(id=1, user_id=1, name="Random name", type="Random type",
                      birthday="2024-01-01", adopted=True, image="default")


def test_create_pet():

    # Arrange
    app.dependency_overrides[PetRepository] = CreatePetQueries
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_account_data
    )
    pet = {
          "name": "string",
          "type": "string",
          "birthday": "2024-01-31",
          "adopted": True,
          "image": "string"
    }

    expected = {
          "id": 1,
          "user_id": 1,
          "name": "string",
          "type": "string",
          "birthday": "2024-01-31",
          "adopted": True,
          "starsign": "N/A",
          "image": "string"
    }

    # Act
    response = client.post("/pets", json=pet)
    app.dependency_overrides = {}

    # Assert

    assert response.status_code == 200
    assert response.json() == expected


def test_get_a_pet():

    # Arrange
    app.dependency_overrides[PetRepository] = EmptyPetRepository
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_account_data
    )
    pet_id_to_get = 1

    # Act
    response = client.get(f"/pets/{pet_id_to_get}")
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "user_id": 1,
        "name": "Random name",
        "type": "Random type",
        "birthday": "2024-01-01",
        "adopted": True,
        "starsign": None,
        "image": "default"
    }
