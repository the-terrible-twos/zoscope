from fastapi.testclient import TestClient
from main import app
from queries.compatibilities import (
    CompatibilityIn,
    CompatibilityOut,
    CompatibilityRepo,
)
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": 1, "username": "testuser"}


class CreateCompatibilityQueries:

    def create(self, compatibility: CompatibilityIn):
        return CompatibilityOut(id=1, user_id=compatibility.user_id,
                                pet_id=compatibility.pet_id, message="message")


def test_create_compatibility():

    # Arrange
    app.dependency_overrides[CompatibilityRepo] = CreateCompatibilityQueries
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_account_data)
    compatibility = {
        "pet_id": 1,
        "user_id": 1
    }

    expected = {
        "id": 1,
        "pet_id": 1,
        "user_id": 1,
        "message": "message"
    }

    # Act
    response = client.post("/compatibilities", json=compatibility)
    app.dependency_overrides = {}

    # Assert

    assert response.status_code == 200
    assert response.json() == expected
