from fastapi.testclient import TestClient
from main import app
from queries.horoscopes import HoroscopeRepository, HoroscopeIn, HoroscopeOut
from authenticator import authenticator


client = TestClient(app)


def fake_get_current_accout_data():
    return {"id": 1, "username": "username"}


class CreateHoroscopeQueries:
    def create(self, horoscope: HoroscopeIn):
        return HoroscopeOut(id=1, pet_id=horoscope.pet_id, fortune="fortune")


def test_create_horoscope():

    # Arrange
    app.dependency_overrides[HoroscopeRepository] = CreateHoroscopeQueries
    app.dependency_overrides[authenticator.get_current_account_data] = (
        fake_get_current_accout_data
        )
    horoscope = {
        "pet_id": 1,
        "created_on": "2024-02-05"
    }

    expected = {
        "id": 1,
        "pet_id": 1,
        "fortune": "fortune"
    }

    # Act
    response = client.post("/horoscopes", json=horoscope)
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == expected
