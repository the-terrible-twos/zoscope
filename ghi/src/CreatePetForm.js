import React, { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import "./styles/global.css";
import "./components/translucent-container.css";
import Banner from "./components/Banner";
import submit_button from "./images/submit_button.png";

function NewPetForm() {
  const [adopted, setAdopted] = useState(false);
  const [type, setType] = useState("text");
  const [formData, setFormData] = useState({
    name: "",
    birthday: "",
    type: "",
  });
  const [image] = useState(
    "https://previews.dropbox.com/p/thumb/ACL6kO8oh7mfe1gaJqKTzKs7MvD4VHhEOu_e4KEGKsDVuUiQbGxB-yqN7zeyt8feEzrPKIJpcpsOenM_jWYKW-CirQVbl-VPseYYtnCIlbVLggfgCHjhfNmGZBpSuDZ1ZGrmAvwp7Lxk0UuX6-y6vqjtBLoKT25cypcV5OyN4XVrH42iaA85VwZ3gEUk8IQ4H3x712qQasv33c6oJZmOEDBkFW8dljh5paGWuBH2MxIhbYmMF7zcmbBuk9XhkwUvBeS9FZktrn9hPcCcC0wmkSsOzExYZ3rfnlENp2-wMemGzrQt551msS7lFXA8csrwjJ_Oz8sEinsVOkU9ELzGkYrI/p.png"
  );

  const { token } = useToken();
  const navigate = useNavigate();

  const handleFormChange = (event) => {
    const { name, value } = event.target;

    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleAdoptedChange = () => {
    setAdopted((prevAdopted) => {
      const newAdopted = !prevAdopted;
      if (newAdopted) {
        const currentDate = new Date();
        const randomYear =
          currentDate.getFullYear() - Math.floor(Math.random() * 10);
        const randomMonth = Math.floor(Math.random() * 12) + 1;
        const randomDay = Math.floor(Math.random() * 28) + 1;
        const formattedDate = `${randomYear}-${String(randomMonth).padStart(
          2,
          "0"
        )}-${String(randomDay).padStart(2, "0")}`;

        setFormData((prevFormData) => ({
          ...prevFormData,
          birthday: formattedDate,
        }));
      } else {
        setFormData((prevFormData) => ({
          ...prevFormData,
          birthday: "",
        }));
      }
      return newAdopted;
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const createPetUrl = `${process.env.REACT_APP_API_HOST}/pets`;
    const data = {
      ...formData,
      adopted,
      image,
    };
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    const response = await fetch(createPetUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: "",
        birthday: "",
        type: "",
      });
      setAdopted(false);
    }
    navigate("/profile");
  };

  return (
    <>
      <div>
        <Banner />
      </div>
      <div className="translucent-container">
        <div>
          <h1 className="text-center">Create a New Pet</h1>
        </div>
        <form onSubmit={handleSubmit} id="create-pet-form">
          <div>
            <input
              value={formData.name}
              onChange={handleFormChange}
              placeholder="Pet Name"
              required
              type="text"
              name="name"
              id="name"
              className="form-control"
            />
          </div>

          <div>
            <select
              value={formData.type}
              onChange={handleFormChange}
              required
              name="type"
              id="type"
              className="form-control"
            >
              <option className="inheritColor" value="Type">Choose a type</option>
              <option className="inheritColor" value="Dog">Dog</option>
              <option className="inheritColor" value="Cat">Cat</option>
              <option className="inheritColor" value="Bird">Bird</option>
              <option className="inheritColor" value="Rabbit">Rabbit</option>
              <option className="inheritColor" value="Snake">Snake</option>
            </select>
          </div>

          <div>
            <input
              value={formData.birthday}
              onChange={handleFormChange}
              placeholder="Birthday"
              type={type}
              onFocus={() => setType("date")}
              onBlur={() => setType("text")}
              name="birthday"
              id="birthday"
              className="form-control"
            />
          </div>

          <div style={{ display: "flex", columnGap: ".25em", margin: "5px" }}>
            <input
              onChange={handleAdoptedChange}
              type="checkbox"
              name="adopted"
              checked={adopted}
              className="toggle-form-control"
            />{" "}
            Adopted? <br />
          </div>

          <div>
            <button className="submit-button">
              <img src={submit_button} alt="Submit" border="0" />
            </button>
          </div>
        </form>
      </div>
    </>
  );
}

export default NewPetForm;
