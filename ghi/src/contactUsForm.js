import React, { useState } from "react";
import "./components/translucent-container.css";
import Banner from "./components/Banner";
import "./styles/global.css";
import submit_button from "./images/submit_button.png";

const ContactForm = () => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [subject, setSubject] = useState('')
    const [message, setMessage] = useState('')


    const handleName = (e) => {
        const value = e.target.value
        setName(value)
    }
    const handleEmail = (e) => {
        const value = e.target.value
        setEmail(value)
    }
    const handleSubject = (e) => {
        const value = e.target.value
        setSubject(value)
    }
    const handleMessage = (e) => {
        const value = e.target.value
        setMessage(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        const formData = { name: name, from_email: email, subject: subject, content: message }
        try {
            const response = await fetch(`${process.env.REACT_APP_API_HOST}/email/send_email/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData)
            })
            const data = await response.json()
            console.log(data)
        } catch (error) {
            console.error('Error:', error)
        }
    }
    return (
        <>
        <Banner />
        <div className="translucent-container">
            <h1>Contact Us</h1>
            <div>
                <form onSubmit={handleSubmit}>
                    <div>
                        <input value={name} onChange={handleName} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    </div>
                    <div>
                        <input value={email} onChange={handleEmail} placeholder="Email" required type="email" name="email" id="email" className="form-control" />
                    </div>
                    <div className="form-floating mb-3">
                        <input value={subject} onChange={handleSubject} placeholder="Subject" required type="text" name="subject" id="subject" className="form-control" />
                    </div>
                    <div>
                        <textarea value={message} onChange={handleMessage} placeholder="Message" required name="message" id="message" className="form-control" style={{ height: '100px' }}></textarea>
                    </div>
                    <button className="submit-button" onClick={handleSubmit}>
                        <img src={submit_button} alt="Submit" border="0" />
                    </button>
                </form>
            </div>
        </div>
        </>
    )
}

export default ContactForm
