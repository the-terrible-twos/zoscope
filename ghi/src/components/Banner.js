import React from "react";
import "./Banner.css";
import zooscopebanner6copy from "../images/zooscopebanner6copy.png";

const Banner = () => {
    return (
        <div className="banner">
            <img src={zooscopebanner6copy} alt="Banner" />
        </div>
    );
};

export default Banner;
