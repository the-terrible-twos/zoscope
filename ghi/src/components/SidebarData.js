import React from "react";
import { CgNotes } from "react-icons/cg";
import { FaUserCircle, FaHome, FaCalendarAlt } from "react-icons/fa";
import { FaPeopleGroup, FaUserPlus } from "react-icons/fa6";
import { MdEmail, MdOutlineLogin, MdOutlineLogout } from "react-icons/md";
import { TbReportAnalytics } from "react-icons/tb";

export const SidebarData = [
    {
        title: 'Sign Up',
        path: '/signup',
        icon: <FaUserPlus />,
        cName: 'nav-text',
        requiresLogin: false
    },
    {
        title: 'Sign In',
        path: '/login',
        icon: <MdOutlineLogin />,
        cName: 'nav-text',
        requiresLogin: false
    },
    {
        title: 'Log Out',
        path: '/',
        icon: <MdOutlineLogout />,
        cName: 'nav-text',
        requiresLogin: true
    },
    {
        title: 'Home',
        path: '/',
        icon: <FaHome />,
        cName: 'nav-text',
        requiresLogin: null
    },
    {
        title: 'My Profile',
        path: '/profile',
        icon: <FaUserCircle />,
        cName: 'nav-text',
        requiresLogin: true
    },
    {
        title: 'Pet Horoscope',
        path: '/report',
        icon: <CgNotes />,
        cName: 'nav-text',
        requiresLogin: null,
    },
    {
        title: 'Compatibility Report',
        path: '/compatibilities',
        icon: <TbReportAnalytics />,
        cName: 'nav-text',
        requiresLogin: true
    },
    {
        title: 'Lunar Calendar',
        path: '/calendar',
        icon: <FaCalendarAlt />,
        cName: 'nav-text',
        requiresLogin: null
    },
    {
        title: 'About Us',
        path: '/about',
        icon: <FaPeopleGroup />,
        cName: 'nav-text',
        requiresLogin: null
    },
    {
        title: 'Contact Us',
        path: '/contact',
        icon: <MdEmail />,
        cName: 'nav-text',
        requiresLogin: null
    },
]
