import React from "react";
import "./DefaultProfileImage.css";
import default_image from "../images/default_image.png";

const DefaultProfileImage = () => {
    return (
        <div className="default_image">
            <img src={ default_image } alt="default_image" />
        </div>
    );
};

export default DefaultProfileImage;
