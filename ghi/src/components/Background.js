import React from "react";
import "./Background.css";
import ZooscopeBackground from "../images/ZooscopeBackground.png"

const Background = () => {
    return (
        <div className="background-container">
            <img src={ZooscopeBackground} alt="Background" />
        </div>
    );
};

export default Background;
