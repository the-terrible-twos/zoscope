import React from "react";
import { FaStar } from "react-icons/fa";
import "./StarButton.css";

function StarButton(props) {
    return (
        <button className="star-button" onClick={props.onClick}>
        {props.children}
        </button>
    );
}

export default StarButton;
