import React, { useState } from "react";
import { FaBars } from "react-icons/fa";
import { IoClose } from "react-icons/io5";
import { Link } from "react-router-dom";
import { SidebarData } from "./SidebarData";
import "./Navbar.css";
import { IconContext } from "react-icons";
import useToken from "@galvanize-inc/jwtdown-for-react";

function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const { token, logout } = useToken();

  const showSidebar = () => setSidebar(!sidebar);
  return (
    <>
      <IconContext.Provider value={{ color: "#fff" }}>
        <div className="navbar">
          {sidebar ? (
            <Link to="#" className="menu-bars" onClick={showSidebar}>
              <IoClose />
            </Link>
          ) : (
            <Link to="#" className="menu-bars" onClick={showSidebar}>
              <FaBars />
            </Link>
          )}
        </div>
        <nav className={sidebar ? "nav-menu active" : "nav-menu"}>
          <ul className="nav-menu-items" onClick={showSidebar}>
            <li className="navbar-toggle">
              {sidebar ? (
                <Link to="#" className="menu-bars">
                  <IoClose />
                </Link>
              ) : null}
            </li>
            {/* eslint-disable */}
            {SidebarData.map((item, index) => {
              if (
                Boolean(token) === item.requiresLogin ||
                item.requiresLogin === null
              ) {
                if (item.title === "Log Out") {
                  return (
                    <li key={index} className={item.cName}>
                      <Link to={item.path} onClick={logout}>
                        {item.icon}
                        <span>{item.title}</span>
                      </Link>
                    </li>
                  );
                }
                return (
                  <li key={index} className={item.cName}>
                    <Link to={item.path}>
                      {item.icon}
                      <span>{item.title}</span>
                    </Link>
                  </li>
                );
              }
            })}
            {/* eslint-enable */}
          </ul>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;
