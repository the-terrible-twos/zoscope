import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./styles/global.css";
import "./components/translucent-container.css";
import Banner from "./components/Banner";
import submit_button from "./images/submit_button.png";

function CompatibilityForm() {
  const [user_id, setUserId] = useState("");
  const [pet_id, setPetId] = useState("");
  const [userFirstName, setUserFirstName] = useState("");
  const [userPets, setUserPets] = useState([]);
  const [userData, setUserData] = useState();
  const [compatibilityResult, setCompatibilityResult] = useState(null);
  const [showForm, setShowForm] = useState(true);
  const { token } = useToken();

  useEffect(() => {
    const fetchToken = async () => {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_HOST}/token`,
          {
            method: "GET",
            credentials: "include",
          }
        );

        if (response.ok) {
          const data = await response.json();
          setUserData(data.account);
        } else {
          console.error("Error fetching token");
        }
      } catch (error) {
        console.error("Error fetching token");
      }
    };

    fetchToken();
  }, []);

  useEffect(() => {
    if (userData) {
      setUserFirstName(userData.first_name);
      setUserId(userData.id);
    }
  }, [userData]);

  useEffect(() => {
    if (token) {
      fetch(`${process.env.REACT_APP_API_HOST}/pets`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => setUserPets(data))
        .catch((error) => console.error("Could not fetch pets:", error));
    }
  }, [token]);

  const handlePetIdChange = (event) => {
    const value = event.target.value;
    setPetId(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.user_id = user_id;
    data.pet_id = pet_id;

    const compatibilityURL = `${process.env.REACT_APP_API_HOST}/compatibilities`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    };

    const response = await fetch(compatibilityURL, fetchConfig);
    if (response.ok) {
      const dataResponse = await response.json();

      setUserId("");
      setPetId("");
      setCompatibilityResult(dataResponse.message);
      setShowForm(false);
    } else {
      console.error("could not create compatibility report");
    }
  };

  return (
    <>
      <Banner />
      <div>
        {showForm && (
          <div className="translucent-container">
            <div>
              <h1 className="text-center">
                Let's See How Compatible You and Your Pet Really Are!!
              </h1>
              <form onSubmit={handleSubmit} id="create-compatibility-form">
                <div>
                  <p>
                    Hello, {userFirstName}! Please select the pet you would like
                    us to create a compatibility report for.
                  </p>
                </div>
                <div>
                  <select
                    value={pet_id}
                    onChange={handlePetIdChange}
                    required
                    name="pet"
                    id="pet"
                    className="form-select"
                  >
                    <option value="pet">Choose your pet</option>
                    {Array.isArray(userPets) &&
                      userPets.map((pet) => (
                        <option key={pet.id} value={pet.id}>
                          {pet.name}
                        </option>
                      ))}
                  </select>
                </div>
                <button className="submit-button" onClick={handleSubmit}>
                  <img
                    src={submit_button}
                    alt="Create Report"
                    border="0"
                  />
                </button>
              </form>
            </div>
          </div>
        )}
        {compatibilityResult && (
          <div className="translucent-container">
            <h1>Compatibility Report</h1>
            <p>{compatibilityResult}</p>
          </div>
        )}
      </div>
    </>
  );
}

export default CompatibilityForm;
