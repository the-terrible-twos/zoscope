import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import HomePage from "./HomePage";
import Navbar from "./components/navbar.js";
import SignupForm from "./SignupForm";
import LoginForm from "./login.js";
import MyProfile from "./MyProfile";
import NewPetForm from "./CreatePetForm.js";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import ContactForm from "./contactUsForm.js";
import LunarCalendar from "./LunarCalendar.js";
import GenHoroscopeForm from "./GenHoroscopeForm.js";
import AboutUs from "./AboutUs.js";
import ResultHoroscope from "./PetResultOfUsers.js";
import EditPet from "./EditPet.js";
import Background from "./components/Background.js";
import CompatibilityForm from "./CompatibilityForm.js";


const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, "");
const baseUrl = process.env.REACT_APP_API_HOST


function App() {
  return (
    <>
      <div className="background-container">
        <Background />
      </div>
      <div>
        <AuthProvider baseUrl= {baseUrl}>
          <BrowserRouter basename={basename}>
            <Navbar />
            <div className="container">
              <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/signup" element={<SignupForm />} />
                <Route path="/login" element={<LoginForm />} />
                <Route path="/profile" element={<MyProfile />} />
                <Route path="/contact" element={<ContactForm />} />
                <Route path="/newPet" element={<NewPetForm />} />
                <Route path="/report" element={<GenHoroscopeForm />} />
                <Route path="/about" element={<AboutUs />} />
                <Route path="/fortune" element={<ResultHoroscope />} />
                <Route path="/pets/:pet_id" element={<EditPet />} />
                <Route path="/calendar" element={<LunarCalendar />} />
                <Route
                  path="/compatibilities"
                  element={<CompatibilityForm />}
                />
              </Routes>
            </div>
          </BrowserRouter>
        </AuthProvider>
      </div>
    </>
  );
}

export default App;
