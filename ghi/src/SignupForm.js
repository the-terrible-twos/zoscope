import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./components/translucent-container.css";
import Banner from "./components/Banner";
import signup_button from "./images/signup_button.png";

function SignupForm() {
  const [type, setType] = useState("text");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [first_name, setFirstName] = useState("");
  const [last_name, setLastName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [password, setPassword] = useState("");
  const { register } = useToken();

  const handleUsername = (event) => {
    const value = event.target.value;
    setUsername(value);
  };

  const handleEmail = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleBirthday = (event) => {
    const value = event.target.value;
    setBirthday(value);
  };
  const handlePassword = (event) => {
    const value = event.target.value;
    setPassword(value);
  };

  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    let data = {};
    data.username = username;
    data.email = email;
    data.password = password;
    data.first_name = first_name;
    data.last_name = last_name;
    data.birthday = birthday;


    const url = `${process.env.REACT_APP_API_HOST}/accounts`;

    try {
      await register(data, url);
      navigate("/");
      alert("Welcome to Zoscope!!!!");
    } catch (error) {
      console.error("Error during registration:", error);

      if (error.status === 400) {
        alert("Username or email already exists. Please choose a new one.");
      } else {
        alert("Error during registration. Please try again.");
      }
    }
  };

  return (
    <>
      <div>
        <Banner />
      </div>
      <div className="translucent-container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Signup</h1>
              <form onSubmit={handleSubmit} id="signup-form">
                <div className="form-floating mb-3">
                  <input
                    value={username}
                    onChange={handleUsername}
                    placeholder="Username"
                    required
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                  />
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={email}
                    onChange={handleEmail}
                    placeholder="Email"
                    required
                    type="text"
                    name="email"
                    id="email"
                    className="form-control"
                  />
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={first_name}
                    onChange={handleFirstName}
                    placeholder="First Name"
                    required
                    type="text"
                    name="first_name"
                    id="first_name"
                    className="form-control"
                  />
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={last_name}
                    onChange={handleLastName}
                    placeholder="Last Name"
                    required
                    type="text"
                    name="last_name"
                    id="last_name"
                    className="form-control"
                  />
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={birthday}
                    onChange={handleBirthday}
                    placeholder="Birthday"
                    type={ type }
                    name="birthday"
                    id="birthday"
                    className="form-control"
                    onFocus={() => setType("date")}
                    onBlur={() => setType("text")}
                  />
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={password}
                    onChange={handlePassword}
                    placeholder="Password"
                    required
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                  />
                </div>
                <button className="signup-button" onClick={handleSubmit}>
                  <img src={ signup_button } alt="Sign Up" border="0" />
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SignupForm;
