import React, { useEffect, useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";
import { css } from '@emotion/react';
import { ScaleLoader } from 'react-spinners';
import "./styles/global.css";


function MyProfile() {
  const [pets, setPets] = useState();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);

  const { token } = useToken();

  useEffect(() => {

    if ( token ){
    fetch(`${process.env.REACT_APP_API_HOST}/pets`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
      })
      .then((response) => response.json())
      .then((data) => setPets(data))
      .catch((error) => console.error("Error fetching pets:", error));
  }
  }, [token]);


  const handleHoroscope = async(pet_id) => {
    try{
      setLoading(true);

      const data = {}
      data.pet_id = pet_id;
      data.created_on = new Date().toISOString().split('T')[0];

      console.log(data)

      const createHoroscopeUrl = `${process.env.REACT_APP_API_HOST}/horoscopes`;

      const fetchConfig = {
        method: "post",
        body:JSON.stringify(data),
        headers: {
          'Content-Type':'application/json',
          Authorization: `Bearer ${token}`,
        },
    };

    const response = await fetch(createHoroscopeUrl, fetchConfig);

    if(response.ok){
      const horoscopeData = await response.json();

      navigate("/fortune", { state: { horoscopeData } });

    }else{
      console.error("Error:", response.status)
    }
    }catch(error){
      console.error("Error", error)
    }finally{
      setLoading(false)
    }
  };


  const override = css`
      display: block;
      margin: 0 auto;
      border-color: red;
    `;

  const handleEditClick = (pet_id) => {
    navigate(`/pets/${pet_id}`);
  };

  const handleDelete = async (pet_id) => {
    try {
      setLoading(true)

      const deletePetUrl = `${process.env.REACT_APP_API_HOST}/pets/${pet_id}`;

      const fetchConfig = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`
        }
      }
      const response = await fetch(deletePetUrl, fetchConfig)

      if (response.ok) {
        setPets(pets.filter(pet => pet.id !== pet_id))
      } else {
        console.error("Error:", response.status)
      }
    } catch (error) {
      console.error("Error:", error)
    } finally {
      setLoading(false)
    }
  }



  return (
    <>
      <h1 className="text-center">My Profile</h1>
      <button className="createPetButton" onClick={() => navigate("/newPet")}>Create new pet</button>
      <div className="sweet-loading text-center">
        <ScaleLoader
          css={override}
          size={150}
          color={"white"}
          loading={loading}
        />
      </div>
    <div className="container text-center d-flex flex-wrap">
        {Array.isArray(pets) &&
          pets.map((pet) => (
            <div className="container-pet-list">
                <div key={pet.id} className="col-md-4 mb-4">
                  <div className="card">
                  <img className="card-img-top"
                    src={pet.image}
                    style={{
                      borderRadius: "50%",
                      width: "250px",
                      height: "180px",
                    }}
                    alt="Pet"
                  />
                <div>
                <h2 style={{fontSize: "25px", margin:"10px"}}>Name: {pet.name}</h2>
                <h4 style={{fontSize: "20px", margin:"10px"}}className="card-text">Starsign: {pet.starsign}</h4>
                <p style={{fontSize: "20px", margin:"10px", fontWeight:"bolder"}}className="card-text">Birthday: {pet.birthday}</p>
                </div>
                <ul className="list-group list-group-flush">
                <li><button className="buttonOfPets" onClick={() => handleEditClick(pet.id)}>Edit</button></li>
                <li><button className="buttonOfPets" onClick={() => handleDelete(pet.id)}>Delete</button></li>
                <li><button className="buttonOfPets" onClick={() => handleHoroscope(pet.id)}>Show Fortune</button></li>
                </ul>
              </div>
            </div>
          </div>
          ))}
      </div>
    </>
  );
}

export default MyProfile;
