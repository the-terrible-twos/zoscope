import useToken from "@galvanize-inc/jwtdown-for-react"
import React, { useState } from "react"
import { useNavigate } from "react-router-dom"
import "./components/translucent-container.css"
import Banner from "./components/Banner"
import signin_button from "./images/signin_button.png";

const LoginForm = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const { login } = useToken()
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const success = await login(username, password)
            e.target.reset()
            if (success) {
                navigate("/")
            } else {
                setErrorMessage("Invalid username or password")
            }
        } catch (error) {
            console.error("Login failed:", error)
            setErrorMessage("An error occurred during login")
        }
    };

    return (
        <>
        <div>
            <Banner />
        </div>
        <div className="translucent-container">
            <h1>Login</h1>
            {errorMessage && <div style={{ color: 'red'}}>{errorMessage}</div>}
            <form onSubmit={(e) => handleSubmit(e)}>
                <div>
                    <input className="form-control" name="username" type="text" placeholder="Username" onChange={(e) => setUsername(e.target.value)} />
                </div>
                <div>
                    <input className="form-control" name="password" type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div>
                    <button className="signin-button">
                        <img src={ signin_button } alt="Sign In" border="0" />
                    </button>
                </div>
            </form>
        </div>
        </>
    )
}

export default LoginForm
