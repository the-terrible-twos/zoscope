// This form should fetch the pet's current data when it loads, fill the form
// with the current values, and send an updated PUT request to the backend when
// the form is submitted

import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from 'react-router-dom'
import useToken from "@galvanize-inc/jwtdown-for-react";
import "./styles/global.css";
import "./components/translucent-container.css";
import Banner from "./components/Banner";
import submit_button from "./images/submit_button.png";

function EditPet() {
    // Extract pet_id from the url
    const { pet_id } = useParams();
    const { token } = useToken();
    const navigate = useNavigate();
    const [pet, setPet] = useState(null);
    const [name, setName] = useState('');
    const [image, setImage] = useState('');

    useEffect(() => {
        //Fetch specific pet data
        fetch(`${process.env.REACT_APP_API_HOST}/pets/${pet_id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            }
        })
        .then(response => response.json())
        .then(data => {
            setPet(data);
            setName(data.name);
            setImage(data.image);
        })
        .catch(error => console.error('Error fetching pet details:', error));
    }, [pet_id, token]);

    const handleNameChange = (event) => setName(event.target.value);
    const handleImageChange = (event) => setImage(event.target.value);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { name, image };

        const response = await fetch(`${process.env.REACT_APP_API_HOST}/pets/${pet_id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        });

        if (response.ok) {
            //Redirect back to profile page
            navigate('/profile');
        } else {
            //Handle errors here
            console.error('Failed to update the pet:', await response.text());
        }
    };

    if (!pet) return <div> Loading... </div>;

    return (
        <>
        <div>
            <Banner />
        </div>
        <div className="translucent-container">
        <div>
            <h1 className="text-center"> Edit Pet</h1>
        </div>
        <div className="text-center">
            <form onSubmit={handleSubmit} id="edit-pet-form">
            <div>
            <input
                id="name"
                placeholder="Pet Name"
                value={name}
                onChange={handleNameChange}
                className="form-control"
            />
            </div>

            <div>
            <input
                id="image"
                placeholder="Pet Image URL"
                value={image}
                onChange={handleImageChange}
                className="form-control"
            />
            </div>

            <button className="submit-button" onClick={handleSubmit}>
                <img src={ submit_button } alt="Submit" border="0" />
            </button>
        </form>
        </div>
        </div>
    </>
    );
}

export default EditPet;
