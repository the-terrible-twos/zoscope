import "./components/translucent-container.css";
import Banner from "./components/Banner";

function AboutUs() {
    return (
        <>
        <div>
            <Banner />
        </div>
        <div className="translucent-container">
            <h1>The scope website was founded in 2024 by The Terribles Two!</h1>
        </div>
        </>
    );
}

export default AboutUs;
