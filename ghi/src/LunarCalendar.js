import React, { useState, useEffect } from 'react';
import "./components/translucent-container.css";
import Banner from "./components/Banner";


function LunarCalendar() {
    const [calendar, setCalendar] = useState("");

    const getData = async () => {
        const response = await fetch("https://moon-phase.p.rapidapi.com/calendar?format=html", {
            method: 'GET',
            headers: {
                'X-RapidAPI-Key': `${process.env.REACT_APP_MOON_API_KEY}`,
                'X-RapidAPI-Host': 'moon-phase.p.rapidapi.com'
            }
        });
            if (response.ok) {
                const data = await response.text();
                setCalendar(data);
            }
            else {
                console.log("could not fetch")
            }
    };

    useEffect(()=> {
        getData();
        }, []);


    return (
      <>
        <div>
          <Banner />
        </div>
        <div className="calendar-translucent-container">
          <div dangerouslySetInnerHTML={{ __html: calendar }} />
        </div>
      </>
    );

}


export default LunarCalendar;
