import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./styles/global.css";
import "./components/translucent-container.css";
import Banner from "./components/Banner";
import submit_button from "./images/submit_button.png";
import again_button from "./images/again_button.png";


function GenHoroscopeForm() {
    const [type, setType] = useState('text')
    const location = useLocation();
    const navigate = useNavigate();
    const [horoscopeResult, setHoroscopeResult] = useState(null);
    const [formData, setFormData] = useState({
        pet_name: "",
        birthday: "",
        type: "",
        adopted: false,
    });


useEffect(() => {
    setHoroscopeResult(null);
    setFormData({
        pet_name: "",
        birthday: "",
        type: "",
        adopted: false
    });
}, [location]);


    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = `${process.env.REACT_APP_API_HOST}/gen_horoscopes/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }


        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const resultData = await response.json();
            setHoroscopeResult(resultData);
            setFormData({
                pet_name: '',
                birthday: '',
                type: '',
                adopted: false
            });
        }
    }


    const handleFormChange = (e) => {
        const { name, type, checked, value } = e.target;
        setFormData({
        ...formData,
        [name]: type === "checkbox" ? checked : value,
        });
    };


    const handleAnotherHoroscope = () => {
        setHoroscopeResult(null);
        navigate("/report");
    };


    if (horoscopeResult) {
        return (
            <>
            <div>
            <Banner />
            </div>
            <div className="translucent-container">
                <h1>{horoscopeResult.pet_name}'s Horoscope</h1>
                <h2>{horoscopeResult.fortune}</h2>
                <button className="again-button" onClick={handleAnotherHoroscope}>
                <img src={ again_button } alt="Again" border="0" />
                </button>
            </div>
            </>
        );
    }


    return (
        <>
        <div>
        <Banner />
        </div>
        <div className="translucent-container">
        <div>
            <h1 className="text-center">Consult the Oracle</h1>
        </div>
        <div className="text-center">
            <form onSubmit={handleSubmit} id="create-gen-horoscope-form">
            <div>
                <input
                value={formData.pet_name}
                onChange={handleFormChange}
                placeholder="Pet Name"
                required
                type="text"
                name="pet_name"
                id="pet_name"
                className="form-control"
                />
            </div>

            <div>
                <input
                value={formData.birthday}
                onChange={handleFormChange}
                placeholder="Birthday"
                required
                type={ type }
                name="birthday"
                id="birthday"
                className="form-control"
                onFocus={ () => setType("date") }
                onBlur={ () => setType("text") }
                />
            </div>

            <div>
                <input
                value={formData.type}
                onChange={handleFormChange}
                placeholder="Pet Type"
                required
                type="text"
                name="type"
                id="type"
                className="form-control"
                />
            </div>

            <div style={{ display: "flex", columnGap: ".25em", margin: "5px"}}>
                <input
                value={formData.adopted}
                onChange={handleFormChange}
                placeholder="Adopted"
                type="checkbox"
                checked={formData.adopted}
                name="adopted"
                id="adopted"
                className="toggle-form-control"
                />
                <label htmlFor="adopted">Adopted?</label>
            </div>

            <button className="submit-button" onClick={handleSubmit}>
                <img src={ submit_button } alt="Submit" border="0" />
            </button>
            </form>
        </div>
        </div>
        </>
    );
}

export default GenHoroscopeForm;
