import React, { useEffect, useState } from 'react';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useLocation } from 'react-router-dom';
import "./styles/global.css";


function ResultHoroscope(){

    const { token } = useToken();
    const [pet, setPet] = useState();
    const location = useLocation();
    const horoscopeData = location.state && location.state.horoscopeData


  useEffect(() => {
      if(horoscopeData && horoscopeData.pet_id){
        fetch(`${process.env.REACT_APP_API_HOST}/pets/${ horoscopeData.pet_id }`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        .then(response => response.json())
        .then(data => {
          setPet(data)
          console.log(token)
        })
        .catch(error => console.error('Error fetching pet details:', error));
      }
    },[horoscopeData, token])


  return (
    <>
    {pet && (
      <div className="text-center">
        <div className="text-below-image">
          <img src={pet.image} style={{ borderRadius: '50%', width: '350px', height: '200px' }} alt="Pet" />
          <div className="translucent-container">
          <h1>Daily fortune for {pet.name}</h1>
          <h2 style={{fontSize: "25px", margin:"10px", fontWeight:"bolder"}}>You are a {pet.starsign}!</h2>
          <h2 style={{fontSize: "25px", margin:"10px", fontWeight:"bolder"}}>{horoscopeData && <div>{horoscopeData.fortune}</div>}</h2>
        </div>
        </div>
      </div>
    )}


  </>
);
}

export default ResultHoroscope;
