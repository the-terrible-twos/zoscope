# Zoscope

- Abigail Shattuck - Full Stack Software Engineer
- Chaos Klopp - Full Stack Software Engineer
- Egemen Sari - Full Stack Software Engineer
- Everest Bradford - Full Stack Software Engineer
- Jane Sundermann - Full Stack Software Engineer

- Zoscope - For all your "fur-tune"-telling needs!


## Design

- [API design](docs/apis.md)
- [Data model](docs/data-model.md)
- [GHI](docs/ghi.md)
- [Integrations](docs/integrations.md)

## Intended market

Intended for those interested in answering their pet's most pressing existential questions on life, death, and the universe.

## Wireframe
-https://excalidraw.com/#room=cbf400aefca1750b080b,0R3os7QEMnLsMqAmheXqug

## Functionality

- Users who sign up for a Zoscope account can:
  - View and update their pet's details on each pet's individual profile page
  - Create their pet's profile with their pet's name, birthday, and image entries
  - View the current year’s calendar with the daily phases of the moon
  - Access the human-pet compatibility form and generate a compatibility message
  - Visitors to the site can do the following without having to sign up for a Zoscope account:
  - Send an email to the Zoscope team through the Contact Us form

## Project Initialization

To use our application on your local machine, please follow these steps:

1. Clone the repository down to your local machine.
2. CD into the new project directory.
3. Run 'docker volume create zodb'.
4. Run 'docker compose build'.
5. Run 'docker compose up'.

### Viewing FastAPI Docs and React Front End

1. To view the FastAPI docs, navigate to https://oct-2023-2-pt-api.mod3projects.com/docs in your internet browser.
2. To view the React-based frontend, navigate to https://the-terrible-twos.gitlab.io/zoscope/ in your internet browser.
3. Because the frontend uses React, store and state can be viewed using the React Google Chrome extensions in the JavaScript console.

## Routing and API Outline

ZoScope is built with fastAPI and allows you to add your pets, discover their star signs, and generate horoscopes for them. Through our site,
you can also learn how compatible you are with your pet based on your star signs. You can add or delete pets from your profile anytime, as well
as update your pet's individual details. Finally, through our site, you have the ability to contact us about any questions or comments you may have for the team.

Our site provides a handy navigation bar, built within each page of the frontend, to provide easy access for all of our application's features. We provide
a breakdown of each frontend url below.

#### React Routes

- **Home Page** `http://localhost:3000`
  - Landing/ Homepage that welcomes you to the site
- **Signup** `http://localhost:3000/signup`
  - Sign up for a ZoScope account
- **Login** `http://localhost:3000/login`
  - Log in to your ZoScope account
- **MyProfile** `http://localhost:3000/profile`
  - View a list of the pets that you have added to your profile
- **Edit Pet Form** `http://localhost:3000/pets/:id`
  - Edit the name or image url of an individual pet
  - Once you submit the form to update your pet, automatically directs you back to MyProfile page
- **Create Pet Form** `http://localhost:3000/newPet`
  - Create and add a pet profile into your acoount
  - Once you submit the form to create your pet, automatically directs you back to MyProfile page
- **Show Daily Fortune** `http://localhost:3000/fortune`
  - Check out the daily horoscope of the pet on your list
  - You can access the page from the pet list in your profile
- **About Us** `http://localhost:3000/about`
  - View the intended use of the website
- **Lunar Calendar** `http://localhost:3000/calendar`
  - View the current year's lunar calendar
- **Compatibility Report** `http://localhost:3000/compatibilities`
- **Show Unregistered User Fortune** `http://localhost:3000/report`
  - Anybody can generate a pet horoscope

  - View Compatibility Report Form and when submitted it will render a compatibility message for the logged in user and their selected pet
